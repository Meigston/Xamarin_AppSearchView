﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections;

namespace AppSearchView
{
    [Activity(Label = "AppSearchView", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private SearchView svPesquisa;
        private ListView lvItens;
        private ArrayAdapter adpConteudo;
        private ArrayList alLista;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            svPesquisa = FindViewById<SearchView>(Resource.Id.svwPesquisa);
            lvItens = FindViewById<ListView>(Resource.Id.lsvResultado);

            AddItens();

            adpConteudo = new ArrayAdapter(this, Android.Resource.Layout.SimpleListItem1, alLista);
            lvItens.Adapter = adpConteudo;

            svPesquisa.QueryTextChange += SvPesquisa_QueryTextChange;
            lvItens.ItemClick += LvItens_ItemClick;
        }

        private void LvItens_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Toast.MakeText(this, adpConteudo.GetItem(e.Position).ToString(), ToastLength.Short).Show();
        }

        private void SvPesquisa_QueryTextChange(object sender, SearchView.QueryTextChangeEventArgs e)
        {
            adpConteudo.Filter.InvokeFilter(e.NewText);
        }

        private void AddItens()
        {
            alLista = new ArrayList();
            alLista.Add("FERRARI PRATA");
            alLista.Add("PORSCHE CARRERA AMARELO");
            alLista.Add("VOLKSVAGEM NEW BETTLE BRANCO");
            alLista.Add("FORD FIESTA PRETO");
            alLista.Add("SUZUKI SCROSS");
            alLista.Add("MITSUBISHI PAJERO FULL");
            alLista.Add("VOLKSVAGEM AMAROK");
        }
    }
}

